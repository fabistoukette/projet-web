var game = new Phaser.Game(800, 600, Phaser.AUTO, 'gameDiv');
var image;
var ball;
var wall1;
var wall2;
var paddles;
var paddle1;
var paddle2;
var scoreText;
var score1 = 0;
var score2 = 0;
var introText;
var ballOnScreenCenter = true;

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('win', winState);

game.state.start('boot');


function checkWinJ1() {
    score1 = score1 + 1;
    if (score1 == 5)
        checkWin();
    else
        reset();
};

function checkWinJ2() {
    score2 = score2 + 1;
    if (score2 == 5)
        checkWin();
    else
        reset();
};

function releaseBall() {
    if (ballOnScreenCenter) {
        ballOnScreenCenter = false;
        //  This gets it moving
        ball.body.velocity.setTo(200, 200);
    }
};

function reset() {
    ballOnScreenCenter = true;
    ball.body.x = 400
    ball.body.y = 200
    paddle1.body.y = 200
    paddle2.body.y = 200
    ball.body.velocity.setTo(0, 0);
};

function checkWin() {
    game.state.start('win');
}