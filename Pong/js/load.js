var loadState = {
    preload: function () {
        var loadingLabel = game.add.text(80,150,'Loading...', {font : '30px Courier', fill: '#ffffff'});

        game.load.image('paddle', 'assets/knocker.png');
        game.load.image('ball', 'assets/ball.png');
        game.load.image('wall', 'assets/wall2.png');
    },
    create: function(){
        game.state.start('menu');
    }
};