var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {
    game.load.image('paddle', 'assets/knocker.png');
    game.load.image('ball', 'assets/ball.png');
    game.load.image('wall', 'assets/wall2.png');
}
var image;
var ball;
var wall1;
var wall2;
var paddles;
var paddle1;
var paddle2;
var scoreText;
var score1 = 0;
var score2 = 0;
var introText;
var ballOnScreenCenter = true;
var score = 0;

function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    cursors = game.input.keyboard.createCursorKeys();
    walls = game.add.group();
    paddles = game.add.group();
    //  This creates a simple sprite that is using our loaded image and
    //  displays it on-screen
    //  and assign it to a variable
    ball = game.add.sprite(400, 200, 'ball');
    wall1 = walls.create(0, 0, 'wall');
    wall2 = walls.create(784, 0, 'wall');
    wall1.alpha = 0;
    wall2.alpha = 0;
    paddle1 = paddles.create(34, 200, 'paddle');
    paddle2 = paddles.create(750, 200, 'paddle');

    ball.enableBody = true;
    walls.enableBody = true;
    game.physics.enable([paddles, ball, walls], Phaser.Physics.ARCADE);

    paddle1.body.immovable = true;
    paddle2.body.immovable = true;

    //  This gets it moving
    ball.body.velocity.setTo(0, 0);

    //  This makes the game world bounce-able
    ball.body.collideWorldBounds = true;
    paddle1.body.collideWorldBounds = true;
    paddle2.body.collideWorldBounds = true;
    //  This sets the image bounce energy for the horizontal 
    //  and vertical vectors (as an x,y point). "1" is 100% energy return
    ball.body.bounce.setTo(1, 1);
    game.input.keyboard.addKeyCapture([
        Phaser.Keyboard.Z,
        Phaser.Keyboard.S,
        Phaser.Keyboard.SPACEBAR
    ]);
    //  The score
    scoreText = game.add.text(385, 16, score1 + ' - ' + score2, { fontSize: '32px', fill: '#fff' });
    introText = game.add.text(250, 400, '-- SPACEBAR to start --', { fontSize: '32px', fill: '#fff' });
}

function update() {
    //  Enable physics between the knocker and the ball
    game.physics.arcade.collide(paddle1, ball);
    game.physics.arcade.collide(paddle2, ball);

    //  Checks to see if the ball overlaps with any of the walls, if he does call the checkWin function
    game.physics.arcade.overlap(ball, wall1, checkWinJ2, null, this);
    game.physics.arcade.overlap(ball, wall2, checkWinJ1, null, this);

    if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
        releaseBall();
    }
    else if (game.input.keyboard.isDown(Phaser.Keyboard.Z)) {
        paddle1.body.velocity.y = -300;
    }
    else if (game.input.keyboard.isDown(Phaser.Keyboard.S)) {
        paddle1.body.velocity.y = 300;
    }
    else {
        paddle1.body.velocity.setTo(0, 0);
    }
    if (cursors.up.isDown) {
        paddle2.body.velocity.y = -300;
    }
    else if (cursors.down.isDown) {
        paddle2.body.velocity.y = 300;
    }
    else {
        paddle2.body.velocity.setTo(0, 0);
    }
}

function render() {
    //debug helper
    game.debug.spriteInfo(ball, 32, 32);
}

function checkWinJ1(ball, wall) {
    score1 = score1 + 1;
    reset();
}

function checkWinJ2(ball, wall) {
    score2 = score2 + 1;
    reset();
}

function releaseBall() {
    if (ballOnScreenCenter) {
        ballOnScreenCenter = false;
        //  This gets it moving
        ball.body.velocity.setTo(200, 200);
        introText.visible = false;
    }
}

function reset() {
    ballOnScreenCenter = true;
    ball.body.x = 400
    ball.body.y = 200
    paddle1.body.y = 200
    paddle2.body.y = 200
    ball.body.velocity.setTo(0, 0);
    introText.visible = true;
    scoreText.text = score1 + ' - ' + score2;
}
