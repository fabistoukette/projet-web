var menuState = {
    create: function () {
        var nameLabel = game.add.text(80, 80, 'PONG', { font: '30px Arial', fill: '#ffffff' });
        var startLabel = game.add.text(80, game.world.height - 80, 'Press SPACEBAR to start', { font: '30px Arial', fill: '#ffffff' });
        var spacebarKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        score1=0;
        score2=0;
        ballOnScreenCenter = true;
        spacebarKey.onDown.addOnce(this.start, this);
    },

    start: function () {
        game.state.start('play');
    }
};