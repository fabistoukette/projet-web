var winState = {
    create: function () {
        var winLabel = game.add.text(80, 80, 'YOU WON!', { font: '50px Arial', fill: '#00FF00' });
        var restartLabel = game.add.text(80, game.world.height - 80, 'Press SPACEBAR to restart', { font: '25px Arial', fill: '#00FF00' });

        var spacebarKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        spacebarKey.onDown.addOnce(this.restart, this);
    },

    restart: function () {
        game.state.start('menu');
    }
}